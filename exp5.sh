#!/bin/bash
factorial=1
i=1

echo "Enter a number to find its factorial: "
read n

while [ $i -le $n ]
do
  factorial=$((factorial * i))
  i=$((i + 1))
done

echo "Factorial of $n is: $factorial"