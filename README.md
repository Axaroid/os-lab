# OS-lab 
A collection of practical lab shell scripts and programs.

## Installation
To use the shell scripts and programs in this project, you will need to have a Unix-based operating system installed, such as Linux or macOS.

## Getting Started
1. Clone the repo
    ```sh
    git clone https://gitlab.com/Axaroid/os-lab.git 
    ```
2. Change the directory
    ```sh
    cd os-lab
    ```
3. You can now access the shell scripts and programs.

## Contributing
1. Fork the repo.
2. Create a new feature branch
    ```sh
    git checkout -b feature/new-script
    ```
3. Add your shell script or program.
4. Commit your changes
    ```sh
    git commit -m "Added xth practical"
    ```
5. Push your changes to your fork
    ```sh
    git push origin feature/new-script
    ```
6. Create a pull request