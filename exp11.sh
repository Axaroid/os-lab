#!/bin/bash

if [ $# -gt 1 ]; then
	echo "Usage: $0 [string]"
	exit 1
fi

input=$1
result=""
len=${#input}

for (( i=$len-1; i>=0; i-- ))
do
	result="$result${input:$i:1}"
done

if [ $input == $result ]
then
	echo "$input is palindrome"
else
	echo "$input is not a palindrome"
fi
