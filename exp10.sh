#!/bin/bash

while true
do
	echo "select a option: "
	echo "a. Display all executable files"
	echo "b. Display all directories"
	echo "c. Display zero sized files"
	echo "d. exit"
	
	read -p "option: " option
	
	case $option in
		a) find . -type f -executable ;;
		b) ls -d */ ;;
		c) find . -type f -size 0 ;;
		d) exit ;;
		*)
			echo "invalid option" ;;
		esac
		
done