#!/bin/bash

if [ $# -lt 1 ]; then
    echo "Usage: $0 [number1] [number2] ... [numberN]"
    exit 1
fi

array=("$@")

for ((i = 0; i < ${#array[@]}; i++)); do
    for ((j = i+1; j < ${#array[@]}; j++)); do
        if (( ${array[$i]} < ${array[$j]} )); then
            temp=${array[$i]}
            array[$i]=${array[$j]}
            array[$j]=$temp
        fi
    done
done

echo "Sorted array in descending order: ${array[@]}"