#!/bin/bash

echo "Enter marks of coa, dcn, os: "
read coa dcn os

total=`expr $coa + $dcn + $os`
percentage=$((total * 100/300))

if [ $percentage -ge 75 ]
then
  class="first class"
elif [ $percentage -ge 65 ]
then
  class="second class"
elif [ $percentage -ge 55 ]
then
  class="third class"
else
  class="fail"
fi

echo "Total marks: $total"
echo "Percentage: $percentage%"
echo "Class: $class"