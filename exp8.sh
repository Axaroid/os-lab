#!/bin/bash

# create a sample file
echo "This is a test file.
It contains some text.
This text is for testing purposes." > testfile.txt

echo "Using grep to search for the word 'test':"
grep "test" testfile.txt

echo "Using fgrep to search for the word 'test':"
fgrep "test" testfile.txt

echo "Using egrep to search for the word 'test':"
egrep "test" testfile.txt

# remove the script after demonstration
rm testfile.txt