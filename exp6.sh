#!/bin/bash
while true
do
  echo "Select an option:"
  echo "a. Display calendar of current month"
  echo "b. Display today's date and time"
  echo "c. Display usernames those are currently logged in the system"
  echo "d. Display your name at given x, y position"
  echo "e. Display your terminal number"
  echo "q. Quit"

  read -p "Option: " option

  case $option in
    a) cal ;;
    b) date ;;
    c) whoami ;;
    d)
      read -p "Enter x coordinate: " x
      read -p "Enter y coordinate: " y
      read -p "Enter your name: " name
      echo -e "\033[${y};${x}H${name}"
      ;;
    e)
      tty ;;
    q)
      exit ;;
    *)
      echo "Invalid option" ;;
  esac

  read -p "Press enter to continue..."
done